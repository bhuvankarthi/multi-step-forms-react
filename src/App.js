import logo from "./logo.svg";
import "./App.css";
import { Component } from "react";
import First from "./components/First";
import Second from "./components/Second";
import Third from "./components/Third";
import Submission from "./components/Submission";

class App extends Component {
  constructor() {
    super();
    this.state = {
      stage: 1,
      isSubmitted: false,
      userdata: {
        firstName: "",
        lastName: "",
        dateOfBirth: "",
        email: "",
        address: "",
      },
      message: {
        message: "",
      },
      checkbox: {
        gender: "",
        radio: "",
        selected: false,
      },
    };
  }

  nextClickHandler = (props) => {
    // console.log(props);
    this.setState({ stage: 2, userdata: props });
  };
  backHandling = () => {
    this.setState({ stage: 1 });
  };
  secondPageHandler = (props) => {
    this.setState({ stage: 3, message: props });
  };
  nextBackHandling = () => {
    this.setState({ stage: 2 });
  };
  submitButtonHandler = () => {
    this.setState({ stage: 0, isSubmitted: true });
  };
  doneHandler = () => {
    this.setState({
      stage: 1,
      isSubmitted: false,
      userdata: {
        firstName: "",
        lastName: "",
        dateOfBirth: "",
        email: "",
        address: "",
      },
      message: {
        message: "",
      },
      checkbox: {
        gender: "",
        radio: "",
        selected: false,
      },
    });
  };

  render() {
    return (
      <div className="main-container">
        {this.state.stage === 1 ? (
          <First
            stage={this.state}
            nextHandler={this.nextClickHandler}
            input={this.state.userdata}
          />
        ) : (
          ""
        )}
        {this.state.stage === 2 ? (
          <Second
            stage={this.state}
            backButtonHandler={this.backHandling}
            secondButton={this.secondPageHandler}
            message={this.state.message}
          />
        ) : (
          ""
        )}
        {this.state.stage === 3 ? (
          <Third
            stage={this.state}
            NextbackButtonHandler={this.nextBackHandling}
            submitButton={this.submitButtonHandler}
            checking={this.state.checkbox}
          />
        ) : (
          ""
        )}
        {this.state.isSubmitted ? (
          <Submission stage={this.state} doneHandle={this.doneHandler} />
        ) : (
          ""
        )}
      </div>
    );
  }
}

export default App;
