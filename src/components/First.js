import moon from "../images/moon.jpg";
import "../css/First.css";

import { Component } from "react";

class First extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      dateOfBirth: "",
      email: "",
      address: "",
      first: false,
      last: false,
      isDateOfBirth: false,
      isEmail: false,
      isAddress: false,
    };
    // this.firstName = this.firstName.bind(this);
  }

  componentDidMount() {
    this.setState({
      ...this.props.input,
    });
  }

  handleNext = (event) => {
    event.preventDefault();
    const { nextHandler } = this.props;
    const { firstName, lastName, dateOfBirth, email, address } = this.state;
    const inputData = { firstName, lastName, dateOfBirth, email, address };

    if (this.state.firstName === "") {
      this.setState({
        first: true,
      });
    } else {
      this.setState({
        first: false,
      });
    }
    if (this.state.lastName === "") {
      this.setState({
        last: true,
      });
    } else {
      this.setState({
        last: false,
      });
    }
    if (this.state.dateOfBirth === "") {
      this.setState({
        isDateOfBirth: true,
      });
    } else {
      this.setState({
        isDateOfBirth: false,
      });
    }
    if (this.state.email === "") {
      this.setState({
        isEmail: true,
      });
    } else {
      this.setState({
        isEmail: false,
      });
    }
    if (this.state.address === "") {
      this.setState({
        isAddress: true,
      });
    } else {
      this.setState({
        isAddress: false,
      });
    }
    if (
      firstName !== "" &&
      lastName !== "" &&
      dateOfBirth !== "" &&
      email !== "" &&
      address !== ""
    ) {
      nextHandler(inputData);
    }
  };

  changeInput = (event) => {
    let target = event.target.name;

    this.setState({ [target]: event.target.value });
  };

  // firstName() {
  //   if (this.state.firstName === "") {
  //     console.log("in");
  //     return <div>This field is mandotory</div>;
  //   }
  // }
  render() {
    return (
      <div className="total-container">
        <img src={moon} className="moon" alt="moon" />
        <div className="info-container">
          <header>
            <div>
              <p className="stage">1</p>
              <p> Sign Up</p>
            </div>
            <div>
              <p className="stage">2</p>
              <p>Message</p>
            </div>
            <div>
              <p className="stage">3</p>
              <p>Checkbox</p>
            </div>
          </header>
          <section className="first-header">
            <p className="step">Step 1/3</p>
            <h3>Sign UP</h3>
          </section>
          <form onSubmit={this.handleNext}>
            <div className="first-last">
              <label>
                First Name
                <br />
                <input
                  type="text-box"
                  placeholder="Your First Name"
                  name="firstName"
                  onChange={(e) => this.changeInput(e)}
                  value={this.state.firstName}
                  // onChange={addValue}
                />
                {this.state.first === true && (
                  <div className="alerts">Enter the valid value</div>
                )}
              </label>

              <label>
                Last Name
                <br />
                <input
                  type="text-box"
                  placeholder="Your Last Name"
                  name="lastName"
                  onChange={(e) => this.changeInput(e)}
                  value={this.state.lastName}
                />
                {this.state.last === true && (
                  <div className="alerts">Enter the valid last name</div>
                )}
              </label>
            </div>
            <div className="birth-mail">
              <label>
                Date of Birth
                <br />
                <input
                  type="date"
                  name="dateOfBirth"
                  onChange={(e) => this.changeInput(e)}
                  value={this.state.dateOfBirth}
                />
                {this.state.isDateOfBirth === true && (
                  <div className="alerts">Enter the date of birth</div>
                )}
              </label>
              <label>
                e-mail
                <br />
                <input
                  type="email"
                  name="email"
                  onChange={(e) => this.changeInput(e)}
                  value={this.state.email}
                />
                {this.state.isEmail === true && (
                  <div className="alerts">Enter the valid email</div>
                )}
              </label>
            </div>
            <div className="address">
              <label>
                Address
                <br />
                <textarea
                  rows="3"
                  cols="40"
                  name="address"
                  onChange={(e) => this.changeInput(e)}
                  value={this.state.address}
                />
                {this.state.isAddress === true && (
                  <div className="alerts">Enter the valid address</div>
                )}
              </label>
            </div>
            <div className="first-Button">
              <button className="next" type="submit">
                Next Step
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default First;
