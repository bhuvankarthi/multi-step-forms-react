import red from "../images/red.jpg";
import "../css/Second.css";
import { Component } from "react";

class Second extends Component {
  constructor() {
    super();
    this.state = {
      message: "",
      isMessage: false,
    };
  }

  componentDidMount() {
    this.setState({
      ...this.props.message,
    });
  }
  backHandler = (event) => {
    event.preventDefault();
    this.props.backButtonHandler();
  };
  secondHandler = (event) => {
    event.preventDefault();
    const { message } = this.state;

    const messageData = { message };
    if (message === "") {
      this.setState({
        isMessage: true,
      });
    } else {
      this.setState({
        isMessage: false,
      });
    }
    if (message !== "") {
      this.props.secondButton(messageData);
    }
  };
  messageUpdate = (event) => {
    this.setState({
      message: event.target.value,
    });
  };
  render() {
    return (
      <div className="second-total-container">
        <img src={red} className="red" alt="red" />
        <div className="second-info-container">
          <header>
            <div>
              <p class="second-stage">✔</p>
              <p> Sign Up</p>
            </div>
            <div>
              <p class="second-stage">2</p>
              <p>Message</p>
            </div>
            <div>
              <p class="second-stage">3</p>
              <p>Checkbox</p>
            </div>
          </header>
          <section class="section">
            <p class="second-step">Step 2/3</p>
            <h3>Message</h3>
          </section>
          <form>
            <div class="second-first-last">
              <label>
                Message
                <br />
                <textarea
                  rows="8"
                  cols="50"
                  value={this.state.message}
                  onChange={this.messageUpdate}
                />
                {this.state.isMessage === true && (
                  <div className="alerts">Message field is mandotory</div>
                )}
              </label>
            </div>

            <div class="second-first-Button">
              <button class="second-back" onClick={this.backHandler}>
                Back
              </button>
              <button class="second-next" onClick={this.secondHandler}>
                Next Step
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Second;
