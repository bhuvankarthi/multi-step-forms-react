import "../css/Submission.css";

function Submission(props) {
  let doneHandler = (event) => {
    event.preventDefault();
    props.doneHandle();
  };
  return (
    <div className="submission-div">
      Your Form is Submitted Successfully
      <div className="tick-div">✅</div>
      <button className="done-button" onClick={doneHandler}>
        Done
      </button>
    </div>
  );
}

export default Submission;
