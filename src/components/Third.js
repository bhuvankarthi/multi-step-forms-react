import boat from "../images/boat.jpg";
import bean from "../images/bean.jpg";
import queen from "../images/queen.jpg";
import "../css/Third.css";
import { Component } from "react";
class Third extends Component {
  constructor() {
    super();
    this.state = {
      gender: "",
      radio: "",
      selected: false,
      imageSelected: false,
      checkBoxSelected: false,
    };
  }

  componentDidMount() {
    this.setState({
      ...this.props.message,
    });
  }
  // queenimage;
  backHandler = (event) => {
    event.preventDefault();
    this.props.NextbackButtonHandler();
  };

  genderHandler = (event) => {
    return new Promise((res, rej) => {
      res(
        this.setState((prevState) => ({
          gender: event.target.alt,
          selected: !prevState.selected,
        }))
      );
    }).then(() => {
      if (this.state.selected === true) {
        event.target.className = "bean-image-selected";
      } else {
        event.target.className = "bean-image";
      }
    });
  };

  radioHandler = (event) => {
    this.setState({
      radio: event.target.value,
    });
  };
  submitHandler = (event) => {
    if (this.state.gender === "") {
      this.setState({
        imageSelected: true,
      });
    } else {
      this.setState({
        imageSelected: false,
      });
    }
    if (this.state.radio === "") {
      this.setState({
        checkBoxSelected: true,
      });
    } else {
      this.setState({
        checkBoxSelected: false,
      });
    }
    if (this.state.gender !== "" && this.state.radio !== "") {
      this.props.submitButton();
    }
  };

  render() {
    return (
      <div className="third-total-container">
        <img src={boat} className="boat" alt="boat" />
        <div className="third-info-container">
          <header>
            <div>
              <p className="third-stage">✔</p>
              <p> Sign Up</p>
            </div>
            <div>
              <p className="third-stage">✔</p>
              <p>Message</p>
            </div>
            <div>
              <p className="third-stage">3</p>
              <p>Checkbox</p>
            </div>
          </header>
          <section className="third-header">
            <p className="step">Step 3/3</p>
            <h3>Checkbox</h3>
          </section>
          <div className="image-check-box" onClick={this.genderHandler}>
            <img src={bean} alt="bean" className={`bean-image `} />
            <img src={queen} alt="queen" className={`queen-image`} />
          </div>
          {this.state.imageSelected === true && (
            <div className="alerts"> Select the Gender</div>
          )}
          <div className="check-div" onClick={this.radioHandler}>
            <hr />
            <label>
              <input
                name="add"
                type="radio"
                className="last-check"
                value="I want to
              add this option"
              />
              I want to add this option
            </label>
            <label>
              <input
                name="add"
                type="radio"
                className="last-check"
                value="Let me click on this checkbox and choose some cool stuff"
              />
              Let me click on this checkbox and choose some cool stuff
            </label>
            <br />
            {this.state.checkBoxSelected === true && (
              <div className="alerts"> choose the valid checkbox</div>
            )}
          </div>
          <div className="button-div">
            <button class="third-back" onClick={this.backHandler}>
              Back
            </button>
            <button class="third-next" onClick={this.submitHandler}>
              Submit
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Third;
